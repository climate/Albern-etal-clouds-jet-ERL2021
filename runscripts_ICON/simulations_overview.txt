ICON-NWP simulations with prescribed SST

locked clouds, interactive water vapor
Temperature (T), and cloud-radiative properties (C) prescribed to 
values from control simulation (1) or from global warming simulation
with uniform +4K SST increase (2)

nanw0005: control simulation with interactive clouds to diagnose the cloud properties
nanw0006: +4K simulation with interactive clouds to diagnose the cloud properties
nanw0007: locked T1C1
nanw0008: locked T2C2
nanw0009: locked T1C2
nanw0010: locked T2C1

locked clouds and locked water vapor
nanw0015: T1C1W1
nanw0016: T1C1W2
nanw0017: T1C2W1
nanw0018: T1C2W2
nanw0019: T2C1W1
nanw0020: T2C1W2
nanw0021: T2C2W1
nanw0022: T2C2W2

Partial Radiative Perturbation Calculation
nanw0023: T1C1 vs. T1C2 (locked clouds)

regional cloud impacts
nanw0031: T1C2TR (4K clouds over tropics, CTL clouds over extratropics)
nanw0032: T1C1TR (CTL clouds over tropics, 4K clouds over extratropics)
nanw0033: T2C2TR
nanw0034: T2C1TR

nanw0044: T1C2ML (4K clouds over midlatitudes, CTL clouds over tropics and polar regions)
nanw0045: T1C1ML (CTL clouds over midlatitudes, 4K clouds over tropics and polar regions)
nanw0046: T2C2ML
nanw0047: T2C1ML

nanw0048: T1C2PO (4K clouds over polar regions, CTL clouds over tropics and midlatitudes)
nanw0049: T1C1PO (CTL clouds over polar regions, 4K clouds over tropics and midlatitudes)
nanw0050: T2C2PO
nanw0051: T2C1PO

nanw0052: T1C2NA (4K clouds over North Atlantic, CTL clouds everywhere else)
nanw0053: T1C1NA (CTL clouds over North Atlantic, 4K clouds everywhere else)
nanw0054: T2C2NA
nanw0055: T2C1NA

nanw0056: T1C2TA (4K clouds over tropical Atlantic, CTL clouds everywhere else)
nanw0057: T1C1TA (CTL clouds over tropical Atlantic, 4K clouds everywhere else)
nanw0058: T2C2TA
nanw0059: T2C1TA

nanw0060: T1C2IO (4K clouds over Indian Ocean, CTL clouds everywhere else)
nanw0061: T1C1IO (CTL clouds over Indian Ocean, 4K clouds everywhere else)
nanw0062: T2C2IO
nanw0063: T2C1IO

nanw0064: T1C2WP (4K clouds over western tropical Pacific, CTL clouds everywhere else)
nanw0065: T1C1WP (CTL clouds over western tropical Pacific, 4K clouds everywhere else)
nanw0066: T2C2WP
nanw0067: T2C1WP

nanw0068: T1C2EP (4K clouds over eastern tropical Pacific, CTL clouds everywhere else)
nanw0069: T1C1EP (CTL clouds over western tropical Pacific, 4K clouds everywhere else)
nanw0070: T2C2EP
nanw0071: T2C1EP

