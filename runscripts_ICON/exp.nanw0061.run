#!/bin/ksh
#=============================================================================
# =====================================
# mistral batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bb1018
#SBATCH --job-name=exp.nanw0061.run
#SBATCH --partition=compute,compute2
#SBATCH --chdir=/work/bb1018/b380490/icon-2.1.00/run
#SBATCH --nodes=8
#SBATCH --threads-per-core=2
#SBATCH --output=/pf/b/b380490/RUN/icon-2.1.00/nanw0061/LOG.exp.nanw0061.run.%j.o
#SBATCH --error=/pf/b/b380490/RUN/icon-2.1.00/nanw0061/LOG.exp.nanw0061.run.%j.o
#SBATCH --exclusive
#SBATCH --time=04:00:00
#SBATCH --mail-user=b380490
#SBATCH --mail-type=BEGIN,END,FAIL

#=============================================================================
#
# ICON run script. Created by ./config/make_target_runscript
# target machine is bullx
# target use_compiler is intel
# with mpi=yes
# with openmp=yes
# memory_model=large
# submit with sbatch -N ${SLURM_JOB_NUM_NODES:-1}
# 
#=============================================================================
set -x
. /work/bb1018/b380490/icon-2.1.00/run/add_run_routines
#-----------------------------------------------------------------------------
# target parameters
# ----------------------------
site="dkrz.de"
target="bullx"
compiler="intel"
loadmodule="intel/16.0 ncl/6.2.1-gccsys cdo/default svn/1.8.13  fca/2.5.2431 mxm/3.4.3082 bullxmpi_mlx/bullxmpi_mlx-1.2.9.2"
with_mpi="yes"
with_openmp="yes"
job_name="exp.nanw0061.run"
submit="sbatch -N ${SLURM_JOB_NUM_NODES:-1}"
#-----------------------------------------------------------------------------
# openmp environment variables
# ----------------------------
export OMP_NUM_THREADS=4
export ICON_THREADS=4
export OMP_SCHEDULE=dynamic,1
export OMP_DYNAMIC="false"
export OMP_STACKSIZE=200M
#-----------------------------------------------------------------------------
# MPI variables
# ----------------------------
mpi_root=/opt/mpi/bullxmpi_mlx/1.2.9.2
no_of_nodes=${SLURM_JOB_NUM_NODES:=1}
mpi_procs_pernode=$((${SLURM_JOB_CPUS_PER_NODE%%\(*} / 2 / OMP_NUM_THREADS))
((mpi_total_procs=no_of_nodes * mpi_procs_pernode))
START="srun --cpu-freq=HighM1 --kill-on-bad-exit=1 --nodes=${SLURM_JOB_NUM_NODES:-1} --cpu_bind=verbose,cores --distribution=block:block --ntasks=$((no_of_nodes * mpi_procs_pernode)) --ntasks-per-node=${mpi_procs_pernode} --cpus-per-task=$((2 * OMP_NUM_THREADS)) --propagate=STACK"
#-----------------------------------------------------------------------------
# load ../setting if exists  
if [ -a ../setting ]
then
  echo "Load Setting"
  . ../setting
fi
#-----------------------------------------------------------------------------
export EXPNAME="nanw0061"
basedir="/scratch/b/b380490/experiments/icon-2.1.00/${EXPNAME}"
#bindir="/work/bb1018/b380490/icon-hdcp2s6-2.1.00_prp/build/x86_64-unknown-linux-gnu/bin"   # binaries
bindir="/work/bb1018/b380490/icon-hdcp2s6-2.1.00_aiko_20190319/build/x86_64-unknown-linux-gnu/bin"   # binaries
# use Aiko'S icon binary for the time being
#bindir="/pf/b/b380459/icon-hdcp2s6-2.1.00/build/x86_64-unknown-linux-gnu/bin"  # binaries

#BUILDDIR=build/x86_64-unknown-linux-gnu
#-----------------------------------------------------------------------------
#=============================================================================
# load profile
if [ -a  /etc/profile ] ; then
. /etc/profile
#=============================================================================
#=============================================================================
# load modules
module purge
module load "$loadmodule"
module list
#=============================================================================
fi
#=============================================================================
export LD_LIBRARY_PATH=/sw/rhel6-x64/netcdf/netcdf_c-4.3.2-parallel-bullxmpi-intel14/lib:$LD_LIBRARY_PATH
#=============================================================================
nproma=16
cdo="cdo"
cdo_diff="cdo diffn"
icon_data_rootFolder="/pool/data/ICON"
icon_data_poolFolder="/pool/data/ICON"
icon_data_buildbotFolder="/pool/data/ICON/buildbot_data"
icon_data_buildbotFolder_aes="/pool/data/ICON/buildbot_data/aes"
icon_data_buildbotFolder_oes="/pool/data/ICON/buildbot_data/oes"
export KMP_AFFINITY="verbose,granularity=core,compact,1,1"
export KMP_LIBRARY="turnaround"
export KMP_KMP_SETTINGS="1"
export OMP_WAIT_POLICY="active"
export OMPI_MCA_pml="cm"
export OMPI_MCA_mtl="mxm"
export OMPI_MCA_coll="^ghc,fca"   # Feb 14, 2019
export MXM_RDMA_PORTS="mlx5_0:1"
export OMPI_MCA_coll_fca_enable="1"
export OMPI_MCA_coll_fca_priority="95"
export MALLOC_TRIM_THRESHOLD_="-1"
ulimit -s 2097152

# this can not be done in use_mpi_startrun since it depends on the
# environment at time of script execution
#case " $loadmodule " in
#  *\ mxm\ *)
#    START+=" --export=$(env | sed '/()=/d;/=/{;s/=.*//;b;};d' | tr '\n' ',')LD_PRELOAD=${LD_PRELOAD+$LD_PRELOAD:}${MXM_HOME}/lib/libmxm.so"
#    ;;
#esac
#!/bin/ksh
#=============================================================================
#
# recommended Namelist settings for pre-operational runs (except for output_nml 
# which may be adapted according to personal needs)
#
# Date: 2013.10.01  Guenther Zangl, Daniel Reinert
#
#=============================================================================
#=============================================================================
#
# This section of the run script containes the specifications of the experiment.
# The specifications are passed by namelist to the program.
# For a complete list see Namelist_overview.pdf
#
# EXPNAME and NPROMA must be defined in as environment variables or must 
# they must be substituted with appropriate values.
#
# DWD, 2010-08-31
#
#-----------------------------------------------------------------------------
#
# Basic specifications of the simulation
# --------------------------------------
#
# These variables are set in the header section of the completed run script:
#
# EXPNAME = experiment name
# NPROMA  = array blocking length / inner loop length
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# The following values must be set here as shell variables so that they can be used
# also in the executing section of the completed run script
#-----------------------------------------------------------------------------
# the namelist filename
atmo_namelist=NAMELIST_${EXPNAME}
#
#-----------------------------------------------------------------------------
# global timing
start_date="1999-01-01T00:00:00Z" #"1989-01-01T00:00:00Z" #"1986-01-01T00:00:00Z" #"1979-01-01T00:00:00Z"		# "mydate_nmlT00:00:00Z"
end_date="2009-01-01T00:00:00Z" #"1999-01-01T00:00:00Z" #"1989-01-01T00:00:00Z"   		# "mydate_nmlT00:00:00Z"

# restart intervals
checkpoint_interval="P6M"
restart_interval="P1Y"

# output intervals
output_interval="PT6H"
file_interval="P1M"

# regular  grid: nlat=96, nlon=192, npts=18432, dlat=1.875 deg, dlon=1.875 deg
reg_lat_def_reg=-89.0625,1.875,89.0625
reg_lon_def_reg=0.,1.875,358.125

#
#-----------------------------------------------------------------------------
# model timing
dtime=720  # 360 sec for R2B6, 120 sec for R3B7

#
#-----------------------------------------------------------------------------
# model parameters
model_equations=3             # equation system
#                     1=hydrost. atm. T
#                     1=hydrost. atm. theta dp
#                     3=non-hydrost. atm.,
#                     0=shallow water model
#                    -1=hydrost. ocean
#-----------------------------------------------------------------------------
# the grid parameters
atmo_dyn_grids="iconR2B04_DOM01.nc"
#-----------------------------------------------------------------------------

# directories definition
#
#ICONDIR=/work/bb1018/b380490/icon-hdcp2s6-2.1.00_prp/			# ${ICON_BASE_PATH}
ICONDIR=/work/bb1018/b380490/icon-hdcp2s6-2.1.00_aiko_20190319/
# use Aiko'S icon bnary for the time being
#ICONDIR=/pf/b/b380459/icon-hdcp2s6-2.1.00/			# ${ICON_BASE_PATH}
RUNSCRIPTDIR=/pf/b/b380490/RUN/icon-2.1.00/${EXPNAME}		# ${ICONDIR}/run

# experiment directory, with plenty of space, create if new
EXPDIR=/scratch/b/b380490/experiments/icon-2.1.00/${EXPNAME} 	# ${ICONDIR}/experiments/${EXPNAME}
if [ ! -d ${EXPDIR} ] ;  then
  mkdir -p ${EXPDIR}
fi
#
ls -ld ${EXPDIR}
if [ ! -d ${EXPDIR} ] ;  then
    mkdir ${EXPDIR}
fi
ls -ld ${EXPDIR}
check_error $? "${EXPDIR} does not exist?"

cd ${EXPDIR}

#-----------------------------------------------------------------------------
#
# write ICON namelist parameters
# ------------------------
# For a complete list see Namelist_overview and Namelist_overview.pdf
#
# ------------------------
# reconstrcuct the grid parameters in namelist form
dynamics_grid_filename=""
for gridfile in ${atmo_dyn_grids}; do
  dynamics_grid_filename="${dynamics_grid_filename} '${gridfile}',"
done

# ------------------------

cat > ${atmo_namelist} << EOF
&parallel_nml
 nproma         = 8  ! optimal setting 8 for CRAY; use 16 or 24 for IBM
 p_test_run     = .false.
 l_test_openmp  = .false.
 l_log_checks   = .false.
 num_io_procs   = 1   ! asynchronous output for values >= 1
 itype_comm     = 1
 iorder_sendrecv = 3  ! best value for CRAY (slightly faster than option 1)
/
&grid_nml
 dynamics_grid_filename  = ${dynamics_grid_filename} !"iconR2B04_DOM01.nc"
 radiation_grid_filename = ""
 dynamics_parent_grid_id = 0
 lredgrid_phys           = .false.
/
&initicon_nml
 init_mode   = 2           ! operation mode 2: IFS
 zpbl1       = 500. 
 zpbl2       = 1000. 
 l_sst_in    = .true.		 ! Jennifer
/
&run_nml
 num_lev        = 47           ! 60
 dtime          = ${dtime}     ! timestep in seconds
 ldynamics      = .TRUE.       ! dynamics
 ltransport     = .true.
 iforcing       = 3            ! NWP forcing
 ltestcase      = .false.      ! false: run with real data
 msg_level      = 7            ! print maximum wind speeds every 5 time steps
 ltimer         = .false.      ! set .TRUE. for timer output
 timers_level   = 1            ! can be increased up to 10 for detailed timer output
 output         = "nml"
/
&nwp_phy_nml
 inwp_gscp       = 1
 inwp_convection = 1
 inwp_radiation  = 1
 inwp_cldcover   = 1
 inwp_turb       = 1
 inwp_satad      = 1
 inwp_sso        = 1
 inwp_gwd        = 1
 inwp_surface    = 1
 icapdcycl       = 3 ! apply CAPE modification to improve diurnalcycle over tropical land (optimizes NWP scores)
 latm_above_top  = .false.  ! the second entry refers to the nested domain (if present)
 efdt_min_raylfric = 7200.
 ldetrain_conv_prec = .false. ! ** new for v2.0.15 **; set .true. to activate detrainment of rain and snow; should be used in R3B08 EU-nest only (not for R2B07)!
 itype_z0         = 2
 icpl_aero_conv   = 1
 icpl_aero_gscp   = 1
 icpl_o3_tp       = 1
 ! resolution-dependent settings - please choose the appropriate one
 ! dt_rad    = 2160. (R2B6) / 1440. (R3B7) ** should be an integer multiple of dt_conv **
 ! dt_conv   = 720. (R2B6) / 360. (R3B7) / 180. (R3B8 - EU-nest)
 ! dt_sso    = 1440. (R2B6) / 720. (R3B7)
 ! dt_gwd    = 1440. (R2B6) / 720. (R3B7)
 lfixvar = .true.
/
&nwp_tuning_nml
 tune_zceff_min = 0.075 ! ** resolution-independent since rev. 25646 **
 itune_albedo   = 1     ! somewhat reduced albedo (w.r.t. MODIS data) over Sahara in order to reduce cold bias
/
&turbdiff_nml
 tkhmin  = 0.75  ! new default since rev. 16527
 tkmmin  = 0.75  !           " 
 pat_len = 750.
 c_diff  = 0.2
 rat_sea = 7.5  ! ** new value since for v2.0.15; previously 8.0 **
 ltkesso = .true.
 frcsmot = 0.2      ! these 2 switches together apply vertical smoothing of the TKE source terms
 imode_frcsmot = 2  ! in the tropics (only), which reduces the moist bias in the tropical lower troposphere
 ! use horizontal shear production terms with 1/SQRT(Ri) scaling to prevent unwanted side effects:
 itype_sher = 3    
 ltkeshs    = .true.
 a_hshr     = 2.0
 icldm_turb = 1     ! ** new recommendation for v2.0.15 in conjunction with evaporation fix for grid-scale rain **
/
&lnd_nml
 ntiles         = 3      !!! operational since March 2015
 nlev_snow      = 3      !!! effective only if lmulti_snow = .true.
 lmulti_snow    = .false. !!! for the time being, until numerical stability issues and coupling with snow analysis are solved
 itype_heatcond = 2
 idiag_snowfrac = 20      !! ** operational since 1.12.15 **
 lsnowtile      = .true.  !! ** operational since 1.12.15 **
 lseaice        = .true.
 llake          = .true.
 frlake_thrhld  = 0.5
 itype_lndtbl   = 3  ! minimizes moist/cold bias in lower tropical troposphere
 itype_root     = 2
 sstice_mode    = 4  			         ! Jennifer
 sst_td_filename = "SST_<year>_<month>_iconR2B04-grid.nc"      ! Jennifer
 ci_td_filename  = "CI_<year>_<month>_iconR2B04-grid.nc"        ! Jennifer
/
&radiation_nml
 irad_o3       = 7
 irad_aero     = 6
 albedo_type   = 2 ! Modis albedo
 vmr_co2       = 390.e-06 ! values representative for 2012
 vmr_ch4       = 1800.e-09
 vmr_n2o       = 322.0e-09
 vmr_o2        = 0.20946
 vmr_cfc11     = 240.e-12
 vmr_cfc12     = 532.e-12
/
&nonhydrostatic_nml
 iadv_rhotheta  = 2
 ivctype        = 2
 itime_scheme   = 4
 exner_expol    = 0.333
 vwind_offctr   = 0.2
 damp_height    = 44000.
 rayleigh_coeff = 1.0   ! R3B7: 1.0 for forecasts, 5.0 in assimilation cycle; 0.5/2.5 for R2B6 (i.e. ensemble) runs
 lhdiff_rcf     = .true.
 divdamp_order  = 24    ! setting for forecast runs; use '2' for assimilation cycle
 divdamp_type   = 32    ! ** new setting for assimilation and forecast runs **
 divdamp_fac    = 0.004 ! use 0.032 in conjunction with divdamp_order = 2 in assimilation cycle
 divdamp_trans_start  = 12500.  ! use 2500. in assimilation cycle
 divdamp_trans_end    = 17500.  ! use 5000. in assimilation cycle
 l_open_ubc     = .false.
 igradp_method  = 3
 l_zdiffu_t     = .true.
 thslp_zdiffu   = 0.02
 thhgtd_zdiffu  = 125.
 htop_moist_proc= 22500.
 hbot_qvsubstep = 19000. ! use 22500. with R2B6
/
&sleve_nml
 min_lay_thckn   = 20.
 max_lay_thckn   = 400.   ! maximum layer thickness below htop_thcknlimit
 htop_thcknlimit = 14000. ! this implies that the upcoming COSMO-EU nest will have 60 levels
 top_height      = 75000.
 stretch_fac     = 0.9
 decay_scale_1   = 4000.
 decay_scale_2   = 2500.
 decay_exp       = 1.2
 flat_height     = 16000.
/
&dynamics_nml
 iequations     = 3
 idiv_method    = 1
 divavg_cntrwgt = 0.50
 lcoriolis      = .TRUE.
/
&transport_nml
 ivadv_tracer  = 3,3,3,3,3
 itype_hlimit  = 3,4,4,4,4
 ihadv_tracer  = 52,2,2,2,2
 iadv_tke      = 0
/
&diffusion_nml
 hdiff_order      = 5
 itype_vn_diffu   = 1
 itype_t_diffu    = 2
 hdiff_efdt_ratio = 24.0
 hdiff_smag_fac   = 0.025
 lhdiff_vn        = .TRUE.
 lhdiff_temp      = .TRUE.
/
&interpol_nml
 nudge_zone_width  = 8
 lsq_high_ord      = 3
 l_intp_c2l        = .true.
 l_mono_c2l        = .true.
 support_baryctr_intp = .false.
/
&extpar_nml
 itopo          = 1
 n_iter_smooth_topo = 1        ! 
 hgtdiff_max_smooth_topo = 0.  ! ** should be changed to 750.,750 with next Extpar update! **
/
&io_nml
 itype_pres_msl = 5  ! New extrapolation method to circumvent Ninjo problem with surface inversions
 itype_rh       = 1  ! RH w.r.t. water
/
&fixvar_nml
 lfixvar_record       = .false. !.true.
 lfixvar_apply        = .true.
 lfixvar_apply_q      = .false.
 lfixvar_apply_c      = .true.
 lfixvar_apply_xcdnc  = .false.
 fixvar_apply_c_expid = 'nanw0005'
 fixvar_apply_c_yoffset = 1 !11 !21
 fixvar_read_dt = 2160.0        ! 36 min
/
!&output_nml
! filetype         =  4                      ! output format: 2=GRIB2, 4=NETCDFv2
! output_start     = "${start_date}"
! output_end       = "${end_date}"
! output_interval  = "PT36M"
! file_interval    = "${file_interval}"
! include_last     = .false.
! output_filename  = '${EXPNAME}-fixvarfields' ! file name base
! filename_format  = "<output_filename>_ML_<datetime2>"
! ml_varlist       = 'xtom','xqm_vap','xqm_liq','xqm_ice','xcld_frc'!,'xcdnc'
! remap            = 0
!/
! OUTPUT: Regular grid, model levels, all domains
&output_nml
 filetype         =  4                        ! output format: 2=GRIB2, 4=NETCDFv2
 output_start     = "${start_date}"
 output_end       = "${end_date}"
 output_interval  = "PT1H"                    !"${output_interval}"
 file_interval    = "${file_interval}"
 include_last     = .false.
 mode             =  2  ! 1: forecast mode (relative t-axis), 2: climate mode (absolute t-axis)
 output_filename  = '${EXPNAME}-2d_ll'           ! file name base
 filename_format  = "<output_filename>_ML_<datetime2>"
 ml_varlist       = 'pres_msl','pres_sfc','t_2m','t_g','tot_prec','tqv','tqc','tqi','clct','clcl','clcm','clch','shfl_s','lhfl_s','qhfl_s','sod_t','sob_t','sou_s','sob_s','thb_t','thu_s','thb_s','swsfcclr','swtoaclr','lwsfcclr','lwtoaclr','tqv_dia','tqc_dia','tqi_dia'
 remap            = 1
 reg_lon_def      = ${reg_lon_def_reg}
 reg_lat_def      = ${reg_lat_def_reg}
/
&output_nml
 filetype         =  4
 output_start     = "${start_date}"
 output_end       = "${end_date}"
 output_interval  = "${output_interval}"
 file_interval    = "${file_interval}"
 include_last     = .false.
 mode             =  2  ! 1: forecast mode (relative t-axis), 2: climate mode (absolute t-axis)
 include_last     =  .false.
 output_filename  = '${EXPNAME}-3d_ll'         ! file name base
 filename_format  = "<output_filename>_PL_<datetime2>"
 pl_varlist       = 'u','v','w','temp','rh','qv','qc','qi','clc','geopot','pv','tot_qv_dia','tot_qc_dia','tot_qi_dia'
 p_levels         = 100000,99000,98000,97000,95000,92500,90000,87000,85000,82000,75000,70000,68000,60000,53000,50000,45000,40000,37000,30000,25000,20000,18000,15000,13000,11000,10000,9000,7500,7000,6000,5000,4500,3500,3000,2800,2100,2000,1500,1000,700,500,300,200,100,50
! p_levels         = 1000,2000,3000,5000,7000,10000,15000,20000,25000,30000,40000,50000,60000,70000,85000,92500,100000
 remap            = 1
 reg_lon_def      = ${reg_lon_def_reg}
 reg_lat_def      = ${reg_lat_def_reg}
/
&output_nml
 filetype         =  4
 output_start     = "${start_date}"
 output_end       = "${end_date}"
 output_interval  = "PT1H"                    !"${output_interval}"
 file_interval    = "${file_interval}"
 include_last     = .false.
 mode             =  2  ! 1: forecast mode (relative t-axis), 2: climate mode (absolute t-axis)
 include_last     =  .false.
 output_filename  = '${EXPNAME}-3d_ll_heatingrates'         ! file name base
 filename_format  = "<output_filename>_PL_<datetime2>"
 pl_varlist       = 'lwflxall','lwflxclr','swflxall','swflxclr','ddt_temp_radsw','ddt_temp_radlw','ddt_temp_radswcs','ddt_temp_radlwcs'
 p_levels         = 100000,99000,98000,97000,95000,92500,90000,87000,85000,82000,75000,70000,68000,60000,53000,50000,45000,40000,37000,30000,25000,20000,18000,15000,13000,11000,10000,9000,7500,7000,6000,5000,4500,3500,3000,2800,2100,2000,1500,1000,700,500,300,200,100,50
 remap            = 1
 reg_lon_def      = ${reg_lon_def_reg}
 reg_lat_def      = ${reg_lat_def_reg}
/
EOF

#!/bin/ksh
#=============================================================================
#
# This section of the run script prepares and starts the model integration. 
#
# bindir and START must be defined as environment variables or
# they must be substituted with appropriate values.
#
# Marco Giorgetta, MPI-M, 2010-04-21
#
#-----------------------------------------------------------------------------
#
# directories definition
# this part is shifted up
#
#-----------------------------------------------------------------------------
# set up the model lists if they do not exist
# this works for subngle model runs
# for coupled runs the lists should be declared explicilty
if [ x$namelist_list = x ]; then
#  minrank_list=(        0           )
#  maxrank_list=(     65535          )
#  incrank_list=(        1           )
  minrank_list[0]=0
  maxrank_list[0]=65535
  incrank_list[0]=1
  if [ x$atmo_namelist != x ]; then
    # this is the atmo model
    namelist_list[0]="$atmo_namelist"
    modelname_list[0]="atmo"
    modeltype_list[0]=1
    run_atmo="true"
  elif [ x$ocean_namelist != x ]; then
    # this is the ocean model
    namelist_list[0]="$ocean_namelist"
    modelname_list[0]="ocean"
    modeltype_list[0]=2
  elif [ x$testbed_namelist != x ]; then
    # this is the testbed model
    namelist_list[0]="$testbed_namelist"
    modelname_list[0]="testbed"
    modeltype_list[0]=99
  else
    check_error 1 "No namelist is defined"
  fi 
fi

#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
# set some default values and derive some run parameteres
restart=${restart:=".false."}
restartSemaphoreFilename='isRestartRun.sem'
#AUTOMATIC_RESTART_SETUP:
if [ -f ${restartSemaphoreFilename} ]; then
  restart=.true.
  #  do not delete switch-file, to enable restart after unintended abort
  #[[ -f ${restartSemaphoreFilename} ]] && rm ${restartSemaphoreFilename}
fi
#END AUTOMATIC_RESTART_SETUP
#
# wait 5min to let GPFS finish the write operations
if [ "x$restart" != 'x.false.' -a "x$submit" != 'x' ]; then
  if [ x$(df -T ${EXPDIR} | cut -d ' ' -f 2) = gpfs ]; then
    sleep 10;
  fi
fi
# fill some checks

run_atmo=${run_atmo="false"}
if [ x$atmo_namelist != x ]; then
  run_atmo="true"
fi
run_jsbach=${run_jsbach="false"}
run_ocean=${run_ocean="false"}

#-----------------------------------------------------------------------------

# link grid, extpar, init and rrtm files
ln -sf /work/bb1018/b380490/inputdata/icon-2.1.00/grids/icon_grid_0010_R02B04_G.nc iconR2B04_DOM01.nc
ln -sf /work/bb1018/b380490/inputdata/icon-2.1.00/realistic/icon_extpar_0010_R02B04_G.nc extpar_iconR2B04_DOM01.nc

ln -sf /work/bb1018/b380490/inputdata/icon-2.1.00/realistic/ifs2icon_R2B04_DOM01.nc ifs2icon_R2B04_DOM01.nc

for year in {1970..2010}; do
for mon in 1 2 3 4 5 6 7 8 9; do 
   ln -sf /work/bb1018/b380490/inputdata/icon-2.1.00/realistic/bc_sst_1979_2008_icongrid_0010_R02B04_mon0${mon}.ymonmean.remapdis.nc  SST_${year}_0${mon}_iconR2B04-grid.nc
   ln -sf /work/bb1018/b380490/inputdata/icon-2.1.00/realistic/bc_sic_1979_2008_icongrid_0010_R02B04_mon0${mon}.ymonmean.remapdis.nc  CI_${year}_0${mon}_iconR2B04-grid.nc
done
for mon in 10 11 12; do 
   ln -sf /work/bb1018/b380490/inputdata/icon-2.1.00/realistic/bc_sst_1979_2008_icongrid_0010_R02B04_mon${mon}.ymonmean.remapdis.nc  SST_${year}_${mon}_iconR2B04-grid.nc
   ln -sf /work/bb1018/b380490/inputdata/icon-2.1.00/realistic/bc_sic_1979_2008_icongrid_0010_R02B04_mon${mon}.ymonmean.remapdis.nc  CI_${year}_${mon}_iconR2B04-grid.nc
done
done

ln -sf /work/bb1018/b380490/icon-2.1.00/data/rrtmg_lw.nc              ./
ln -sf /work/bb1018/b380490/icon-2.1.00/data/rrtmg_sw.nc              ./
ln -sf /work/bb1018/b380490/icon-2.1.00/data/ECHAM6_CldOptProps.nc    ./

# link fixvar input fields
for year in {2000..2009}; do
for mon in 1 2 3 4 5 6 7 8 9; do
   ln -sf /scratch/b/b380490/experiments/icon-2.1.00/nanw0061_fixvar_input/fixvarfields_IOCTL_${year}0${mon}01T000000Z.nc fixvar_nanw0005_${year}0${mon}.nc
done
for mon in 10 11 12; do
   ln -sf /scratch/b/b380490/experiments/icon-2.1.00/nanw0061_fixvar_input/fixvarfields_IOCTL_${year}${mon}01T000000Z.nc fixvar_nanw0005_${year}${mon}.nc
done
done
ln -sf /scratch/b/b380490/experiments/icon-2.1.00/nanw0061_fixvar_input/fixvarfields_IOCTL_20100101T000000Z.nc fixvar_nanw0005_201001.nc

#-----------------------------------------------------------------------------
# print_required_files
copy_required_files
link_required_files


#-----------------------------------------------------------------------------
# get restart files

if  [ x$restart_atmo_from != "x" ] ; then
  rm -f restart_atm_DOM01.nc
#  ln -s ${ICONDIR}/experiments/${restart_from_folder}/${restart_atmo_from} ${EXPDIR}/restart_atm_DOM01.nc
  cp ${ICONDIR}/experiments/${restart_from_folder}/${restart_atmo_from} cp_restart_atm.nc
  ln -s cp_restart_atm.nc restart_atm_DOM01.nc
  restart=".true."
fi
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
#
# create ICON master namelist
# ------------------------
# For a complete list see Namelist_overview and Namelist_overview.pdf

#-----------------------------------------------------------------------------
# create master_namelist
master_namelist=icon_master.namelist
if [ x$end_date = x ]; then
cat > $master_namelist << EOF
&master_nml
 lrestart            = $restart
/
&time_nml
 ini_datetime_string = "$start_date"
 dt_restart          = $dt_restart
 is_relative_time    = .false.
/
EOF
else
if [ x$calendar = x ]; then
  calendar='proleptic gregorian'
  calendar_type=1
else
  calendar=$calendar
  calendar_type=$calendar_type
fi
cat > $master_namelist << EOF
&master_nml
 lrestart            = $restart
/
&master_time_control_nml
 calendar             = "$calendar"
 checkpointTimeIntval = "$checkpoint_interval" 
 restartTimeIntval    = "$restart_interval" 
 experimentStartDate  = "$start_date" 
 experimentStopDate   = "$end_date" 
/
&time_nml
 is_relative_time = .false.
/
EOF
fi
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
# add model component to master_namelist
add_component_to_master_namelist()
{
    
  model_namelist_filename="$1"
  model_name=$2
  model_type=$3
  model_min_rank=$4
  model_max_rank=$5
  model_inc_rank=$6
  
cat >> $master_namelist << EOF
&master_model_nml
  model_name="$model_name"
  model_namelist_filename="$model_namelist_filename"
  model_type=$model_type
  model_min_rank=$model_min_rank
  model_max_rank=$model_max_rank
  model_inc_rank=$model_inc_rank
/
EOF

}
#-----------------------------------------------------------------------------

no_of_models=${#namelist_list[*]}
echo "no_of_models=$no_of_models"

j=0
while [ $j -lt ${no_of_models} ]
do
  add_component_to_master_namelist "${namelist_list[$j]}" "${modelname_list[$j]}" ${modeltype_list[$j]} ${minrank_list[$j]} ${maxrank_list[$j]} ${incrank_list[$j]}
  j=`expr ${j} + 1`
done

#-----------------------------------------------------------------------------
#
#  get model
#
export MODEL=${bindir}/icon
#
ls -l ${MODEL}
check_error $? "${MODEL} does not exist?"
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#
# start experiment
#

rm -f finish.status
date
${START} ${MODEL}
date
if [ -r finish.status ] ; then
  check_error 0 "${START} ${MODEL}"
else
  check_error -1 "${START} ${MODEL}"
fi
#
#-----------------------------------------------------------------------------
#
finish_status=`cat finish.status`
echo $finish_status
echo "============================"
echo "Script run successfully: $finish_status"
echo "============================"
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
namelist_list=""
#-----------------------------------------------------------------------------
# check if we have to restart, ie resubmit
#   Note: this is a different mechanism from checking the restart
if [ $finish_status = "RESTART" ] ; then
  echo "restart next experiment..."
  this_script="${RUNSCRIPTDIR}/${job_name}"
  echo 'this_script: ' "$this_script"
  touch ${restartSemaphoreFilename}
  cd ${RUNSCRIPTDIR}
  ${submit} $this_script
else
  [[ -f ${restartSemaphoreFilename} ]] && rm ${restartSemaphoreFilename}
fi

#-----------------------------------------------------------------------------
# automatic call/submission of post processing if available
if [ "x${autoPostProcessing}" = "xtrue" ]; then
  # check if there is a postprocessing is available
  cd ${RUNSCRIPTDIR}
  targetPostProcessingScript="./post.${EXPNAME}.run"
  [[ -x $targetPostProcessingScript ]] && ${submit} ${targetPostProcessingScript}
  cd -
fi

#-----------------------------------------------------------------------------
# check if we test the restart mechanism
get_last_1_restart()
{
  model_restart_param=$1
  restart_list=$(ls *restart_*${model_restart_param}*_*T*Z.nc)
  
  last_restart=""
  last_1_restart=""  
  for restart_file in $restart_list
  do
    last_1_restart=$last_restart
    last_restart=$restart_file

    echo $restart_file $last_restart $last_1_restart
  done  
}


restart_atmo_from=""
restart_ocean_from=""
if [ x$test_restart = "xtrue" ] ; then
  # follows a restart run in the same script
  # set up the restart parameters
  restart_from_folder=${EXPNAME}
  # get the previous from last rstart file for atmo
  get_last_1_restart "atm"
  if [ x$last_1_restart != x ] ; then
    restart_atmo_from=$last_1_restart
  fi
  get_last_1_restart "oce"
  if [ x$last_1_restart != x ] ; then
    restart_ocean_from=$last_1_restart
  fi
  
  EXPNAME=${EXPNAME}_restart
  test_restart="false"
fi

#-----------------------------------------------------------------------------

cd $RUNSCRIPTDIR

#-----------------------------------------------------------------------------

	
# exit 0
#
# vim:ft=sh
#-----------------------------------------------------------------------------
