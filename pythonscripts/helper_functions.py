import numpy as np
import numpy.ma as ma
import matplotlib.colors as mcolors
import pandas as pd
import netCDF4 as nc
from scipy.optimize import curve_fit

# create new colormap based on ncl colormap BlueYellowRed.rgb
def generate_mymap():
    cmap_name='BlueYellowRed'
    colortab=pd.read_csv(cmap_name + '.rgb',
                         delim_whitespace=True, skiprows=2)
    colortab=colortab.values[:,0:3]/255
    colortab_new = np.full((colortab.shape[0], 4), np.nan, dtype=float)
    colortab_new[:, 0:3] = colortab
    colortab_new[:,3] = 1
    mymap = mcolors.LinearSegmentedColormap.from_list(cmap_name, colortab_new, N=254)
    del colortab_new
    # create another colormap with white space at 3 center points of colormap
    colortab_new2 = np.full((colortab.shape[0]+2, 4), np.nan, dtype=float)
    colortab_new2[0:126, 0:3] = colortab[0:126, :]
    colortab_new2[126:129, 0:3] = np.array([[1, 1, 1], [1,1,1], [1,1,1]]) # change center of colorbar to white
    colortab_new2[129:, 0:3] = colortab[127:, :]
    colortab_new2[:,3] = 1
    mymap2 = mcolors.LinearSegmentedColormap.from_list(cmap_name, colortab_new2, N=254)
    del colortab_new2
    del colortab, cmap_name
    
    return mymap, mymap2

######################################################################
######################################################################
# calculate monthly mean and seasonal mean data
def calcMonthlyandSeasonMean(data, months, seasons):
    """ Calculate the monthly mean and season mean of a given time series.
    
    Use a time series of monthly means of a variable to calculate the mean
    over all Januarys, Februarys etc. and for the seasons for the whole
    time series.
    
    Args:
        data: numpy ndarray with monthly data.
        months: List with abbreviations for all 12 months.
        seasons: List with abbreviations for the 4 seasons.

    Returns:
        monthly_mean: Mean over all months for whole time series.
        season_mean: Mean over seasons for whole time series.
    """
    
    # dictionary to be able to identify which month belongs to which season
    seasons_dict = {'DJF': ['Dec', 'Jan', 'Feb'],
                    'MAM': ['Mar', 'Apr', 'May'],
                    'JJA': ['Jun', 'Jul', 'Aug'],
                    'SON': ['Sep', 'Oct', 'Nov']
                    }

    monthly_data = {}
    monthly_mean = {}
    for n, month in enumerate(months):
        monthly_data[month] = data[n::12, :]
            # mean over every twelth month for every n -> one mean for all 
            # 12 months
        monthly_mean[month] = np.nanmean(monthly_data[month], axis=0)

    # the season mean considers all DJF months, even JF at beginning of time
    # series and D at its end
    season_mean = {}
    for season in seasons:
        season_mean[season] = np.nanmean([ monthly_mean[month] for month in \
                                           seasons_dict[season] ], axis=0)

    return monthly_mean, season_mean

######################################################################
######################################################################
# jet latitude and jet strength
def func_fit_quadratic(x, p0, p1, p2):
    return p0+p1*x+p2*x**2

def get_eddyjetlatint(u, lat):
    # calculate latitude of eddy-driven jet 
    # make sure that lat is ordered from SP to NP; otherwise
    # np.arange does not work to create latint
    if lat[0] > lat[1]:
        lat = lat[::-1]
        u = u[::-1]

    # Southern hemisphere
    # search for jet between 25S and 70S
    indlat_sh = np.squeeze(np.array(np.where((lat<-25.0) & (lat>-70.0))))
    # find index of maximum value of u within the defined latitude range
    maxlat = np.argmax(u[indlat_sh]) + indlat_sh[0]
    # do quadratic fit around the maximum
    # interpolate latitude and u-wind for a more precise result
    latint=np.arange(lat[maxlat-1], lat[maxlat+1], 0.01)
    uint = np.interp(latint, lat[indlat_sh], u[indlat_sh])
    p,_ = curve_fit(func_fit_quadratic, latint, uint)
    ufit = func_fit_quadratic(latint, p[0], p[1], p[2])
    jetlat_sh = latint[np.argmax(ufit)]
    # get strength of jet (maximum of u850 at jet latitude)
    jetint_sh = ufit[np.argmax(ufit)] 

    # delete variables that are not returned by the function, so that you can
    # use the same names for the northern hemisphere
    del indlat_sh, maxlat, latint, uint, p, ufit

    # Northern hemisphere
    # search for jet between 25N and 70N
    indlat_nh = np.squeeze(np.array(np.where((lat>25.0) & (lat<70.0))))
    # find index of maximum value of u within the defined latitude range
    maxlat = np.argmax(u[indlat_nh]) + indlat_nh[0]
    # do a quadratic fit around the maximum
    # interpolate latitude and u-wind for a more precise result
    latint=np.arange(lat[maxlat-1], lat[maxlat+1], 0.01)
    uint = np.interp(latint, lat[indlat_nh], u[indlat_nh])
    p,_ = curve_fit(func_fit_quadratic, latint, uint)
    ufit = func_fit_quadratic(latint, p[0], p[1], p[2])
    jetlat_nh = latint[np.argmax(ufit)]

    # get strength of jet (maximum of u850 at jet latitude)
    jetint_nh = ufit[np.argmax(ufit)] # = ufit.max()

    del indlat_nh, maxlat, latint, uint, p, ufit
   
    return jetlat_sh, jetint_sh, jetlat_nh, jetint_nh

# same function as above, but only for northern hemisphere
def get_eddyjetlatint_NH(u, lat):
    # calculate latitude of eddy-driven jet
    # make sure that lat is ordered from SP to NP; otherwise
    # np.arange does not work to create latint
    if lat[0] > lat[1]:
        lat = lat[::-1]
        u = u[::-1]
    # Northern hemisphere
    # search for jet between 25N and 70N
    indlat_nh = np.squeeze(np.array(np.where((lat>25.0) & (lat<70.0))))
    # find index of maximum value of u within the defined latitude range
    maxlat = np.argmax(u[indlat_nh]) + indlat_nh[0]
    # do a quadratic fit around the maximum
    # interpolate latitude and u-wind for a more precise result
    latint=np.arange(lat[maxlat-1], lat[maxlat+1], 0.01)
    uint = np.interp(latint, lat[indlat_nh], u[indlat_nh])
    p,_ = curve_fit(func_fit_quadratic, latint, uint)
    ufit = func_fit_quadratic(latint, p[0], p[1], p[2])
    jetlat_nh = latint[np.argmax(ufit)]

    # get strength of jet (maximum of u850 at jet latitude)
    jetint_nh = ufit[np.argmax(ufit)] # = ufit.max()
   
    return jetlat_nh, jetint_nh

######################################################################
######################################################################
# adapt function for detection of jet latitude and jet strength in 
# Northern Hemisphere so that it can treat NaN's.
def get_eddyjetlatint_NH_nan(u, lat, lo):
    # calculate latitude of eddy-driven jet 
    # make sure that lat is ordered from SP to NP; otherwise
    # np.arange does not work to create latint
    if lat[0] > lat[1]:
        lat = lat[::-1]
        u = u[::-1]
    # Northern Hemisphere
    if lo > 0 and lo < 40: # avoid diagnosing the jet in the Mediterranean sea
                           # and Black Sea
        indlat_nh = np.squeeze(np.array(np.where((lat>45.0) & (lat<62.0))))
    else:
        indlat_nh = np.squeeze(np.array(np.where((lat>25.0) & (lat<62.0))))
    # find index of maximum value of u within the defined latitude range
    maxlat = np.nanargmax(u[indlat_nh]) + indlat_nh[0]
    # do a quadratic fit around the maximum
    # interpolate latitude and u-wind for a more precise result
    try:
        latint=np.arange(lat[maxlat-1], lat[maxlat+1], 0.01)
        uint = np.interp(latint, lat[indlat_nh], u[indlat_nh])
        p,_ = curve_fit(func_fit_quadratic, latint, uint)
        ufit = func_fit_quadratic(latint, p[0], p[1], p[2])
        jetlat_nh = latint[np.nanargmax(ufit)]

        # get strength of jet (maximum of u850 at jet latitude)
        jetint_nh = ufit[np.nanargmax(ufit)] # = ufit.max()
        
    except ValueError:
        print("Value error in function get_eddyjetlatint_NH_nan!\n" + \
              " Set jetlat and jetint to NaN for longitude " + str(lo))
        jetlat_nh = np.nan
        jetint_nh = np.nan
       
    return jetlat_nh, jetint_nh

######################################################################
######################################################################
# get total response, SST impact and cloud impact from cloud locking
# simulations
def calc_impacts_timmean(var_T1C1, var_T2C2, var_T1C2, var_T2C1):
    """ Decompose climate warming response of a given variable into total
        response, SST impact and cloud impact.
    
    Calculate the global warming response of a time-mean variable based on 
    cloud-locking method. Decompose
    the (total) response into a contribution of changes in cloud-radiative
    properties and a contribution of changes in SSTs (SST increase). You need
    four simulations. SST and clouds are taken either from control climate 
    (T1, C1) or from global warming simulation (T2, C2).
        
    Args:
        var_T1C1: lat-lon array with time-mean data of T1C1 simulation.
        var_T2C2: lat-lon array with time-mean data of T2C2 simulation.
        var_T1C2: lat-lon array with time-mean data of T1C2 simulation.
        var_T2C1: lat-lon array with time-mean data of T2C1 simulation.

    Returns:
        tot: total response of variable to global warming.
        sst: SST impact on global warming response.
        cld: cloud impact on global warming response.
    """
    # calculate climate change response of time mean variable
    # total response of circulation
    tot = var_T2C2 - var_T1C1
    # response to SST increase (mean over both climates)
    sst = np.nanmean(np.array([(var_T2C1 - var_T1C1),
                               (var_T2C2 - var_T1C2)]), axis=0)
    # response to clouds (mean over both climates)
    cld = np.nanmean(np.array([(var_T1C2 - var_T1C1),
                               (var_T2C2 - var_T2C1)]), axis=0)
    return tot, sst, cld


######################################################################
######################################################################
# get total response, SST impact, cloud impact and water vapor impact
# from cloud- and water vapor-locking simulations
def calc_3impacts_timmean(var_T1C1W1, var_T2C2W2, var_T1C2W2, var_T2C1W1,
                          var_T1C2W1, var_T1C1W2, var_T2C2W1, var_T2C1W2):
    """ Decompose climate warming response of a given variable into total
        response, SST impact, cloud-radiative impact and water vapor (or clouds
        in different region) radiative impact.
    
    Calculate the global warming response of a time-mean variable based on 
    cloud-locking method. Decompose the (total) response into a contribution
    of changes in cloud-radiative properties, changes in water vapor (or 
    clouds in a different region) and changes in SSTs (SST increase). You need
    eight simulations. SST, clouds and water vapor (clouds in different region)
    are taken either from control climate (T1, C1, W1) or from global warming
    simulation (T2, C2, W2).
        
    Args:
        var_T1C1W1: lat-lon array with time-mean data of T1C1W1 simulation.
        var_T2C2W2: lat-lon array with time-mean data of T2C2W2 simulation.
        var_T1C2W2: lat-lon array with time-mean data of T1C2W2 simulation.
        var_T2C1W1: lat-lon array with time-mean data of T2C1W1 simulation.
        var_T1C2W1: lat-lon array with time-mean data of T1C2W1 simulation.
        var_T1C1W2: lat-lon array with time-mean data of T1C1W2 simulation.
        var_T2C2W1: lat-lon array with time-mean data of T2C2W1 simulation.
        var_T2C1W2: lat-lon array with time-mean data of T2C1W2 simulation.

    Returns:
        tot: total response of variable to global warming.
        sst: SST impact on global warming response.
        cld: cloud-radiative impact on global warming response.
        vap: water vapor radiative impact or impact from clouds in a different
             region than in cld on global warming response.
    """
    # calculate climate change response of time mean variable
    # total response of circulation
    tot = var_T2C2W2 - var_T1C1W1
    # response to SST increase (mean over both climates)
    sst = np.nanmean(np.array([(var_T2C1W1 - var_T1C1W1),
                               (var_T2C2W2 - var_T1C2W2)]), axis=0)
    # response to clouds (mean over both climates)
    cld = np.nanmean(np.array([(var_T1C2W1 - var_T1C1W1),
                               (var_T1C2W2 - var_T1C1W2),
                               (var_T2C2W1 - var_T2C1W1),
                               (var_T2C2W2 - var_T2C1W2)]), axis=0)
    # response in water vapor or clouds in a different region than in cld
    # (mean over both climates)
    vap = np.nanmean(np.array([(var_T1C1W2 - var_T1C1W1),
                               (var_T1C2W2 - var_T1C2W1),
                               (var_T2C1W2 - var_T2C1W1),
                               (var_T2C2W2 - var_T2C2W1)]), axis=0)
    
    return tot, sst, cld, vap

######################################################################
######################################################################
# copy of basemap function shiftgrid, because basemap could not be installed
def shiftgrid_copy(lon0,datain,lonsin,start=True,cyclic=360.0):
    if np.fabs(lonsin[-1]-lonsin[0]-cyclic) > 1.e-4:
        # Use all data instead of raise ValueError, 'cyclic point not included'
        start_idx = 0
    else:
        # If cyclic, remove the duplicate point
        start_idx = 1
    if lon0 < lonsin[0] or lon0 > lonsin[-1]:
        raise ValueError('lon0 outside of range of lonsin')
    i0 = np.argmin(np.fabs(lonsin-lon0))
    i0_shift = len(lonsin)-i0
    if ma.isMA(datain):
        dataout  = ma.zeros(datain.shape,datain.dtype)
    else:
        dataout  = np.zeros(datain.shape,datain.dtype)
    if ma.isMA(lonsin):
        lonsout = ma.zeros(lonsin.shape,lonsin.dtype)
    else:
        lonsout = np.zeros(lonsin.shape,lonsin.dtype)
    if start:
        lonsout[0:i0_shift] = lonsin[i0:]
    else:
        lonsout[0:i0_shift] = lonsin[i0:]-cyclic
    dataout[...,0:i0_shift] = datain[...,i0:]
    if start:
        lonsout[i0_shift:] = lonsin[start_idx:i0+start_idx]+cyclic
    else:
        lonsout[i0_shift:] = lonsin[start_idx:i0+start_idx]
    dataout[...,i0_shift:] = datain[...,start_idx:i0+start_idx]
    return dataout,lonsout


######################################################################
######################################################################
# calcualte the zonal mean over a given region
def calcBoxZonalmean(var, lats, lons, lonwest, loneast):
    """ Calculate the zonal-mean of a given variable for a 
    region specified by lonwest and loneast.

    Calculate the zonal-mean of a given variable for a region
    specified by lonwest and loneast.

    Args:
        var: lat-lon array.
        lats: array with latitudes.
        lons: array with longitudes.
        lonwest: western longitude.
        loneast: eastern longitude.

    Returns:
        var_zm: time-mean zonal-mean variable for the given region.
    """

    # check that longitudes range from 0 to 360
    if lons.min() < 0:
        print('longitudes range from ' + str(lons.min()) + ' to ' + \
              str(lons.max()))
        print('Leave function calcOceanbasinMean')
        return

    if lonwest >= 0: # keep lons from 0 to 360
        # index of lonwest and loneast in longitude vector
        lonind_west = (np.abs(lons-lonwest)).argmin()
        lonind_east = (np.abs(lons-loneast)).argmin()

        var_zm = np.nanmean(var[:, lonind_west:lonind_east+1], axis=1)
        del lonind_west, lonind_east
        
    elif lonwest < 0: # shift longitudes from 0...360 deg lon to -180...180 deg
        var1, lons1 = shiftgrid_copy(180., var, lons, start=False)
        lonind_west = (np.abs(lons1-lonwest)).argmin()
        lonind_east = (np.abs(lons1-loneast)).argmin()
        
        var_zm = np.nanmean(var1[:, lonind_west:lonind_east+1], axis=1)
        del lonind_west, lonind_east
        
    return var_zm

######################################################################
######################################################################

# calculate vertical means based on level indices
def get_verticalmean_overp_tropo(data, plev, tropoind1, tropoind2):
    # input data must have shape: levs, lats, lons
    
    # levels must go from TOA to surface
    if plev[0] > plev[1]:
        print('change order of levels to go from TOA to surface')
        plev = plev[::-1]
        data = data[::-1, :, :]
    
    nlev  = plev.size
    dplev = np.full(nlev, np.nan, dtype=float)
    dplev[1:nlev-1] = 0.5*(np.abs(plev[2:nlev]-plev[0:nlev-2]))
    dplev[0] = np.abs(plev[1]-plev[0])
    dplev[nlev-1] = np.abs(plev[nlev-1]-plev[nlev-2])
        
    out = np.full(data[0,:,:].shape, np.nan, dtype=float)
    for la in range(data[0,:,0].shape[0]): # latitudes
        for lo in range(data[0,0,:].shape[0]): # longitudes
            out[la,lo] = np.nansum(data[tropoind1[la,lo]:tropoind2[la,lo]+1,
                                        la, lo] * \
                                   dplev[tropoind1[la,lo]:tropoind2[la,lo]+1],
                                   axis=0) / \
                         np.nansum(dplev[tropoind1[la,lo]:tropoind2[la,lo]+1])
        del lo
    del la
    return out


######################################################################
######################################################################
# read a specified variable for a certain pressure level
def read_var_onelevel(ifile, var, varlev, level, printmessages=False):
    ncfile = nc.Dataset(ifile, 'r')
    lats = ncfile.variables['lat'][:].data
    lons = ncfile.variables['lon'][:].data
    levs = ncfile.variables[varlev][:].data
    var_nc = ncfile.variables[var][:].data
    ncfile.close()
    del ncfile
    
    # shift longitudes, so that they range from 0 to 360 and not from -180 to 180
    if lons.min() < 0 or lons.max() < 190:
        if printmessages:
            print('      shift longitudes')
        var_nc, lons = obm.shiftgrid_copy(0., var_nc, lons, start=False)
        lons = lons + 360

    # check that latitudes are from south to north
    if lats[0] > lats[1]:
        if printmessages:
            print('      shift latitudes')
        lats = lats[::-1]
        var_nc = var_nc[:, :, ::-1, :]

    # get variable for specified pressure level
    levind = (np.abs(levs-level*1e2)).argmin() # index of level
    var_lev = var_nc[:, levind, :, :]
    del levs, var_nc, levind

    return var_lev, lats, lons

##############################################################################
##############################################################################
# read zonal wind at 850hPa from file that contains only this variable
#def read_u850(ifile, var, printmessages=False):
#    ncfile = nc.Dataset(ifile, 'r')
#    lats = ncfile.variables['lat'][:].data
#    lons = ncfile.variables['lon'][:].data
#    var_nc = np.squeeze(ncfile.variables[var][:].data)
#    ncfile.close()
#    del ncfile
#    
#    # Set high values to NaN's.
#    var_nc[var_nc > 1000] = np.nan
#    if printmessages:
#        print('     ', np.nanmax(var_nc), np.nanmin(var_nc),
#              " contains NaN's:", np.isnan(var_nc).any())
#    
#    # check latitudes and longitudes
#    # longitudes should range from 0 to 360 and not from -180 to 180
#    if lons.min() < 0 or lons.max() < 190:
#        if printmessages:
#            print('      shift longitudes')
#        var_nc, lons = obm.shiftgrid_copy(0., var_nc, lons, start=False)
#        lons = lons + 360
#
#    # latitudes should be from south to north
#    if lats[0] > lats[1]:
#        if printmessages:
#            print('      shift latitudes')
#        lats = lats[::-1]
#        var_nc = var_nc[:, ::-1, :]
#
#    return var_nc, lats, lons


##############################################################################
##############################################################################
# horizontal interpolation of the data

# this is a copy of the mpl_toolkits.basemap.interp function
def interp_copy(datain,xin,yin,xout,yout,checkbounds=False,masked=False,order=1):
    # xin and yin must be monotonically increasing.
    if xin[-1]-xin[0] < 0 or yin[-1]-yin[0] < 0:
        raise ValueError('xin and yin must be increasing!')
    if xout.shape != yout.shape:
        raise ValueError('xout and yout must have same shape!')
    # check that xout,yout are
    # within region defined by xin,yin.
    if checkbounds:
        if xout.min() < xin.min() or \
           xout.max() > xin.max() or \
           yout.min() < yin.min() or \
           yout.max() > yin.max():
            raise ValueError('yout or xout outside range of yin or xin')
    # compute grid coordinates of output grid.
    delx = xin[1:]-xin[0:-1]
    dely = yin[1:]-yin[0:-1]
    if max(delx)-min(delx) < 1.e-4 and max(dely)-min(dely) < 1.e-4:
        # regular input grid.
        xcoords = (len(xin)-1)*(xout-xin[0])/(xin[-1]-xin[0])
        ycoords = (len(yin)-1)*(yout-yin[0])/(yin[-1]-yin[0])
    else:
        # irregular (but still rectilinear) input grid.
        xoutflat = xout.flatten(); youtflat = yout.flatten()
        ix = (np.searchsorted(xin,xoutflat)-1).tolist()
        iy = (np.searchsorted(yin,youtflat)-1).tolist()
        xoutflat = xoutflat.tolist(); xin = xin.tolist()
        youtflat = youtflat.tolist(); yin = yin.tolist()
        xcoords = []; ycoords = []
        for n,i in enumerate(ix):
            if i < 0:
                xcoords.append(-1) # outside of range on xin (lower end)
            elif i >= len(xin)-1:
                xcoords.append(len(xin)) # outside range on upper end.
            else:
                xcoords.append(float(i)+(xoutflat[n]-xin[i])/(xin[i+1]-xin[i]))
        for m,j in enumerate(iy):
            if j < 0:
                ycoords.append(-1) # outside of range of yin (on lower end)
            elif j >= len(yin)-1:
                ycoords.append(len(yin)) # outside range on upper end
            else:
                ycoords.append(float(j)+(youtflat[m]-yin[j])/(yin[j+1]-yin[j]))
        xcoords = np.reshape(xcoords,xout.shape)
        ycoords = np.reshape(ycoords,yout.shape)
    # data outside range xin,yin will be clipped to
    # values on boundary.
    if masked:
        xmask = np.logical_or(np.less(xcoords,0),np.greater(xcoords,len(xin)-1))
        ymask = np.logical_or(np.less(ycoords,0),np.greater(ycoords,len(yin)-1))
        xymask = np.logical_or(xmask,ymask)
    xcoords = np.clip(xcoords,0,len(xin)-1)
    ycoords = np.clip(ycoords,0,len(yin)-1)
    # interpolate to output grid using bilinear interpolation.
    if order == 1:
        xi = xcoords.astype(np.int32)
        yi = ycoords.astype(np.int32)
        xip1 = xi+1
        yip1 = yi+1
        xip1 = np.clip(xip1,0,len(xin)-1)
        yip1 = np.clip(yip1,0,len(yin)-1)
        delx = xcoords-xi.astype(np.float32)
        dely = ycoords-yi.astype(np.float32)
        dataout = (1.-delx)*(1.-dely)*datain[yi,xi] + \
                  delx*dely*datain[yip1,xip1] + \
                  (1.-delx)*dely*datain[yip1,xi] + \
                  delx*(1.-dely)*datain[yi,xip1]
    elif order == 0:
        xcoordsi = np.around(xcoords).astype(np.int32)
        ycoordsi = np.around(ycoords).astype(np.int32)
        dataout = datain[ycoordsi,xcoordsi]
    elif order == 3:
        try:
            from scipy.ndimage import map_coordinates
        except ImportError:
            raise ValueError('scipy.ndimage must be installed if order=3')
        coords = [ycoords,xcoords]
        dataout = map_coordinates(datain,coords,order=3,mode='nearest')
    else:
        raise ValueError('order keyword must be 0, 1 or 3')
    if masked:
        newmask = ma.mask_or(ma.getmask(dataout), xymask)
        dataout = ma.masked_array(dataout, mask=newmask)
        if not isinstance(masked, bool):
            dataout = dataout.filled(masked)
    return dataout

def interpolate2d(datao, lato, lono, lati, loni):
    # datao is input of assumed dimension (ntim, nlato, nlono)
    # lato, lono is old input grid
    # o means "old"
    
    # dimensions of time
    ntim = datao.shape[0]
    
    # dimensions of new latitude and longitude
    nlati = lati.size
    nloni = loni.size
    
    # lato, lono must be increasing
    if (not(np.all(np.diff(lato)>0))):
        raise ValueError()    
    if (not(np.all(np.diff(lono)>0))):
        raise ValueError()

    # prepare interpolated data array
    datai = np.full((ntim, nlati, nloni), np.nan, dtype=float)
    
    # interpolate lat-lon dimension
    x, y = np.meshgrid(loni, lati)
    for t in range(ntim):
        datai[t,:,:] = interp_copy(datao[t,:,:], lono, lato, x, y,
                                   checkbounds=False, masked=False, order=1)
    del t
    
    return datai