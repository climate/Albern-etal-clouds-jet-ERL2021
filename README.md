Documentation of plotting and analysis scripts for N. Albern, A. Voigt, and J. G. Pinto: "Tropical cloud-radiative changes contribute to robust climate change-induced jet exit strengthening over Europe during boreal winter", submitted to ERL in March 2021.

The plotting and analysis scripts and the resulting figures are provided in the directory "pythonscripts".

The run scripts for the ICON-NWP simulations (version 2.1.00), which were performed at the Mistral supercomputer of the German Climate Computing Center (DKRZ) in Hamburg, Germany, are provided in the directory "runscripts_ICON".

The ICON, MPI-ESM and IPSL-CM5A data used for the figures will be published at KITOpen from Karlsruhe Institute of Technology.
